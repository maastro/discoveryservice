# MIA Discovery Service (Eureka)


The [Spring Cloud Netflix Discovery Service](https://en.wikipedia.org/wiki/Service_discovery) is used to register all microservices used in the [Medical Image Analysis framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home).

## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser 