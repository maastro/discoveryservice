package nl.maastro.mia.discoveryserver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.maastro.mia.discoveryserver.EurekaDiscoveryApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = EurekaDiscoveryApplication.class)
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

}
